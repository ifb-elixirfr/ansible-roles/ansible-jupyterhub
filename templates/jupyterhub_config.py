c.JupyterHub.db_url = '{{ jupyterhub_db_url }}'
c.JupyterHub.allow_named_servers = {{ jupyterhub_allow_named_servers }}
c.JupyterHub.named_server_limit_per_user = {{ jupyterhub_named_server_limit_per_user }}

c.JupyterHub.cleanup_proxy = {{ jupyterhub_cleanup_proxy }}
c.JupyterHub.cleanup_servers = {{ jupyterhub_cleanup_servers }}

c.JupyterHub.hub_bind_url = 'http://{{ ansible_default_ipv4.address }}:8081'
c.JupyterHub.hub_ip = '0.0.0.0'

c.Authenticator.admin_users = [{% for admin in jupyterhub_admin_users %}'{{ admin }}',{% endfor %}]
c.JupyterHub.admin_access = {{ jupyterhub_admin_access }}

c.Spawner.default_url = '/lab'
c.Spawner.notebook_dir = '{{ jupyterhub_notebook_dir }}'

batch_script = '''#!/bin/bash
#SBATCH --account={{ '{{' }}account{{ '}}' }}
#SBATCH --partition={{ '{{' }}partition{{ '}}' }}
#SBATCH --time={{ '{{' }}runtime{{ '}}' }}
#SBATCH --output={{ '{{' }}homedir{{ '}}' }}/.jupyter_%j.log
#SBATCH --job-name=jupyter
#SBATCH --chdir={{ '{{' }}homedir{{ '}}' }}
#SBATCH --cpus-per-task={{ '{{' }}nprocs{{ '}}' }}
#SBATCH --mem={{ '{{' }}memory{{ '}}' }}
#SBATCH --get-user-env=L
#SBATCH --export={{ '{{' }}keepvars{{ '}}' }}{% if jupyterhub_rserver_support is defined and jupyterhub_rserver_support %},RSERVER_CONFIG{% endif %}

{% raw %}{% if gres %}#SBATCH --gres={{gres}}{% endif %}{% endraw %}

#SBATCH {{ '{{' }}options{{ '}}' }}

{% if jupyterhub_custom_path is defined %}
export PATH={{jupyterhub_custom_path}}:$PATH
{% endif %}

export PATH={{jupyterhub_latex_path}}:/shared/software/jupyterhub/bin:$PATH

{% if jupyterhub_rserver_support is defined and jupyterhub_rserver_support %}
export PATH={{ jupyterhub_rserver_path }}:$PATH
export RSERVER_CONFIG={{ jupyterhub_home }}/etc/rserver.conf
{% endif %}

which jupyterhub-singleuser
echo {{ '{{' }}cmd{{ '}}' }}
srun {{ '{{' }}cmd{{ '}}' }} --log-level={{ jupyterhub_cmd_log_level }}
'''

c.JupyterHub.spawner_class = 'slurmspawner.FormSlurmSpawner'
c.FormSlurmSpawner.req_runtime = '12:00:00'
c.FormSlurmSpawner.batch_script = batch_script
c.Spawner.http_timeout = 120
