ansible-jupyterhub
=======================

This role installs and configures JupyterHub to run over a SLURM cluster.

Requirements
------------

To run JupyterHub you will need
* A SLURM cluster
* A server (for instance a VM) :
  * running Ubuntu 18.04
  * configured as a SLURM client
  * with PAM already configured to authenticate your users
* A shared storage between the cluster nodes and the server

Role Variables
--------------

| Variable                               | Default                                    | Description                                                                                                                                                                                  |
|----------------------------------------|--------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| jupyterhub_miniconda_path              | /shared/software/miniconda                 | Path of the miniconda to use.<br>If the path does not exist, miniconda will be installed to the specified location.<br>This should be located on a shared storage accross the cluster nodes. |
| jupyterhub_home                        | /shared/software/jupyterhub                | Path for the installation of JupytherHub.<br>This should be located on a shared storage accross the cluster nodes.                                                                           |
| jupyterhub_db_url                      | sqlite:///opt/jupyterhub/jupyterhub.sqlite | DB Url in SQLAlchemy format (postgresql is recommanded)                                                                                                                                      |
| jupyterhub_allow_named_servers         | true                                       | Allow named servers                                                                                                                                                                          |
| jupyterhub_named_server_limit_per_user | 5                                          | Named server limit per user                                                                                                                                                                  |
| jupyterhub_admin_users                 | []                                         | List of admin users logins                                                                                                                                                                   |
| jupyterhub_profiles                    | []                                         | List of batch spawner profiles (see `defaults/main.yml` for more details)                                                                                                                    |

Dependencies
------------

This role as no dependencies

Example Playbook
----------------


    - hosts: all
      vars:
        miniconda_path: /opt/miniconda
        jupyterhub_home: /opt/jupyterhub
        jupyterhub_db_url: sqlite:///opt/jupyterhub/jupyterhub.sqlite
        jupyterhub_admin_users: [alice, john]
      roles:
        - role: ansible-jupyterhub

License
-------

GPL-2.0-or-later

Author Information
------------------

Julien Seiler (@julozi)
